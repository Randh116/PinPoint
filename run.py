from flask import session
from flask_socketio import SocketIO
import time
from application import create_app
from application.models import db
from application.chat import save_message
import config

# app = create_app()

app = create_app()
socketio = SocketIO(app)


@socketio.on('event')
def handle_my_custom_event(json, methods=['GET', 'POST']):
    """
    Funkcja odpowiedzialna za zapisywanie wiadomości
    oraz za wysyłanie ich do innych klientów.
    :param json: json
    :param methods: POST GET
    :return: None
    """

    data = dict(json)
    if "user_id" and "room_id" in data:
        save_message(data["user_id"], data["room_id"], data["content"])

    socketio.emit('message response', json)


if __name__ == '__main__':
    # app.run(debug=True)
    socketio.run(app, debug=True, host=str(config.Config.SERVER))
