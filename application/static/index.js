/*
    w folderze static umieszczamy wszystkie skrypty ktore sa statyczne
    tzn maja wplyw na nasze strony ale one sa niezmienne
    np. skrypty js'a, arkusze css etc.
 */

function deleteRoom(room_id){
    fetch("/delete-room", {
        method: "POST",
        body: JSON.stringify({room_id: room_id}),
    }).then((_res) => {
        window.location.href = "/rooms";
    });
}


function deleteNote(note_id){
    fetch("/delete-note", {
        method: "POST",
        body: JSON.stringify({note_id: note_id}),
    }).then((_res) => {
        window.location.href = "/notes";
    });
}


function editNote(note_id){
    fetch("/edit-note", {
        method: "POST",
        body: JSON.stringify({note_id: note_id}),
    }).then((_res) => {
        window.location.href = "/add-note";
    });
}


function addMessage(user_id, room_id){
    fetch("/add-message", {
        method: "POST",
        body: JSON.stringify({
            user_id: user_id,
            room_id: room_id,
        }),
    }).then((_res) => {
        console.log("jestem w addMessage js")
        window.location.href = "/chatroom/" + room_id;
    });
}


$(function(){
    $('#sendBtn').bind('click', function () {
        var value = document.getElementById("chatroommsg").value;
        console.log(value)
        $.getJSON('/send-message',
            {val: value},
            function(data) {

            });
        return false;
    });
    fetch('/get-messages')
        .then(function (response) {
            return response.text();
        }).then(function (text) {

    });
    return false;
});


