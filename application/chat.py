from flask import Blueprint, render_template, request, flash, redirect, url_for, jsonify, session
from flask_login import current_user, login_required
from .models import db, User, Room, Message, Participant
import json
from datetime import datetime
from .chat_server import client


chat = Blueprint('chat', __name__)
client_obj = None

MSG_LIMIT = 20

@chat.route('/add-room', methods=['GET', 'POST'])
def add_room():
    if request.method == 'POST':
        roomname = request.form.get('roomname')
        password = request.form.get('password')
        roomtype = request.form.get('roomtype')
        user_limit = request.form.get('userlimit')
        accesstype = request.form.get('accesstype')
        owner_id = current_user.user_id

        room = Room.query.filter_by(roomname=roomname).first()
        if room:
            flash('Pokój o podanej nazwie już istnieje!', category='error')
        else:
            new_room = Room(roomname=roomname,
                            password=password,
                            roomtype=roomtype,
                            user_limit=user_limit,
                            accesstype=accesstype,
                            owner_id=owner_id)
            db.session.add(new_room)
            db.session.commit()

            flash('Pokój został utworzony!', category='success')

            return redirect(url_for('views.rooms'))

    return render_template("chat/add_room.html", user=current_user)


@chat.route('/delete-room', methods=['GET', 'POST'])
def delete_room():
    room = json.loads(request.data)

    room_id = room['room_id']

    room = Room.query.get(room_id)
    if room:
        if room.owner_id == current_user.user_id:
            db.session.delete(room)
            db.session.commit()

            flash('Pokój został pomyślnie usunięty.', category='success')
        else:
            flash('Nie możesz usunąć pokoju, którego nie jesteś właścicielem!', category='error')

    return jsonify({})


@chat.route('/chatroom/<room_id>/enter', methods=['GET', 'POST'])
def enter_chatroom(room_id):
    global client_obj

    client_obj = client.Client(current_user.username)

    user_id = current_user.user_id
    room_id = int(room_id)

    new_participant = Participant(user_id=user_id,
                                  room_id=room_id)
    db.session.add(new_participant)
    db.session.commit()

    flash('Wejście do pokoju zakończone pomyślnie.', category='success')

    new_participantJSON = new_participant.as_dict()
    session["participant"] = new_participantJSON

    return render_template("chat/chatroom.html", user=current_user, room_id=room_id, **{"session": session})


@chat.route('/chatroom/<room_id>/leave', methods=['GET', 'POST'])
def leave_chatroom(room_id):
    global client_obj

    if client_obj:
        client_obj.disconnect()

    user_id = current_user.user_id
    room_id = int(room_id)
    participant = Participant.query.filter_by(user_id=user_id, room_id=room_id).first()

    if participant:
        db.session.delete(participant)
        db.session.commit()

        session["participant"] = None

        flash('Wyjście z pokoju zakończone pomyślnie.', category='success')

        return redirect(url_for('views.rooms'))

    return render_template("chat/chatroom.html", user=current_user, room_id=room_id)


@chat.route('/send-message', methods=['GET'])
def send_message():
    global client_obj

    participantJSON = json.dumps(session["participant"])
    participant = json.loads(participantJSON)

    user_id = current_user.user_id
    room_id = participant["room_id"]

    msg = request.args.get("val")

    new_message = Message(content=msg,
                          date=datetime.now(),
                          is_saved=True,
                          user_id=user_id,
                          room_id=room_id)

    db.session.add(new_message)
    db.session.commit()

    if client_obj:
        client_obj.send_message(msg)

    return jsonify({})


@chat.route('/get-messages')
def get_messages():
    return get_all_messages(to_json=True)


@chat.route('/user/get-name')
def get_name():
    data = {"name": ""}

    if current_user.is_authenticated:
        data = {"name": current_user.username}

    return jsonify(data)


@chat.route('/history/user/<user_id>/messages')
def history_user_message(user_id):
    json_messages = get_all_messages(user_id)

    return render_template("chat/history.html", user=current_user, messages=json_messages)


@chat.route('/history/chatroom/<room_id>/messages')
def history_room_messages(room_id):
    json_messages = get_all_messages(room_id)

    return render_template("chat/history.html", user=current_user, messages=json_messages)


@chat.route('/history/chatroom/<room_id>/user/<user_id>/messages')
def history_room_user_messages(user_id, room_id):
    json_messages = get_all_messages(user_id, room_id)

    return render_template("chat/history.html", user=current_user, messages=json_messages)


def get_all_messages(user_id=None, room_id=None, limit=100, to_json=False):

    if not user_id and not room_id:
        all_messages = Message.query.limit(limit).all()
    if not user_id and room_id:
        all_messages = Message.query.filter_by(room_id=room_id).limit(limit).all()
    if user_id and not room_id:
        all_messages = Message.query.filter_by(user_id=user_id).limit(limit).all()
    else:
        all_messages = Message.query.filter_by(user_id=user_id, room_id=room_id).limit(limit).all()

    if to_json:
        all_messages_asdict = []

        for message in all_messages:
            all_messages_asdict.append(message.as_dict())

        all_messagesJSON = json.dumps(all_messages_asdict, indent=4, sort_keys=True, default=str)

        return all_messagesJSON
    else:
        return all_messages


